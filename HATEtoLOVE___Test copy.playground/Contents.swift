/*:
 ![alternate text ](GamePromo.png)
 Before you initiate Playground, make sure that it's in **FULL-SCREEN LANDSCAPE** mode to get the best experience out of it.
 
 __What is HATEtoLOVE all about?__
 
 HATEtoLOVE is a 4 letter word Game with two simple Rule.
 
 -> Change single alphabet at a time (**H**ATE -> **L**ATE)
 
 -> Reach target word by Changing one letter at a time
 
 Example,
 
 **H**ATE -> **L**ATE
 
 L**A**TE -> L**I**TE
 
 LI**T**E -> LI**V**E
 
 LI**V**E -> L**O**VE
 
 
 __How to Play on Swift Playground?__
 
 
 ![alternate text ](HelpImage.png)
 Just like shown above, scribble on current Word. For example, While going towards hate to love, you can write L on H to make HATE as LATE.
 
 It's that simple!
 
 Red color indicates that word you made is invalid and green makes it Valid. You can use Hint while you are stuck or skip the level entirely.
 
 __Sometime it doesn't recognise word correctly, what's going wrong?__
 
 Model is trained to get better accuracy. But in some case like below one you might get undesired result.
 
 - Scribble Capitalized Alphabets Only
 
 - Scribble too Small (Solution: try to write on center with bigger size)
 
 - Written word that does not exist in dictionary I have (Solution: Think of other word or get Hint until I update Dictionary)
 
 - Moved hand too fast or maybe didn't draw character on one go (Solution: We have 3 minutes, don't hurry)
 
 - Model didn't recognise character Properly (Solution: Waiting for WWDC19 to have in-built recognition tools)
 
 
 __What's most exciting thing about this playground?__
 
 Under the hood, all of this is made from Circle, Rectanges, Text and Images. And that makes it so interesting.
 ![alternate text ](linePath.png)
 As you can see in picture,Everything you write is processed as circle and line and then mapped directly as 2D array from it's location. And then CoreML does it's magic from the model which was trained on python. Alongside, you see A boy and Girl which really gave a theme to this simple word Game. Those different pose were entirely created by me from keychain I have. And in recent times, when I showed these to my friends that random puzzle based on difficulty mode is generated, they couldn't believe. They feel amazed after I explained them how I did it.

 __Anything you want to Add?__
 
I want to quickly share few pictures I took during this development week before you jump into HATEtoLOVE.
 ![CraftProcess (Histogram of Effort) ](craftprocess.gif)
 ![CraftProcess (Wheel)](craft0.jpg)
 ![Pictures snapshot](craft1.jpg)
 ![Brainstorming](craft2.jpg)
 ![A boy, ready to style up](craft3.jpg)
 And finally, it's me. (That way maybe You may recognise me at Event in case I get selected in my 3rd and final Attempt)
 ![alternate text ](shivamAgrawal.jpg)
 
 */

import UIKit
import CoreML
import PlaygroundSupport
import  AVFoundation
import Foundation
import CoreML

//setup playground
_setup()
Canvas.shared.color = .white

//Setting Up Fonts
let cfURL = Bundle.main.url(forResource: "Shivam_handwritten_bold-Regular", withExtension: "ttf")! as CFURL
CTFontManagerRegisterFontsForURL(cfURL, CTFontManagerScope.process, nil)
let boldFontName = String("Shivam_Handwritten_Bold-Rg")

//sound Environment Setup
do{
    try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient, mode: .default, options: .duckOthers)
    try AVAudioSession.sharedInstance().setActive(true)
}
catch{
    print("Failed to set sound environment to play multiple sound")
}

//Function to play Various button sound
var buttonSoundPlayer = AVAudioPlayer()
public func playButtonSound(filename: String,loopCount: Int) {
    DispatchQueue.main.asyncAfter(deadline: .now()){
        let url = Bundle.main.url(forResource: filename, withExtension: nil)
        guard let newURL = url else {
            print("Could not find file: \(filename)")
            return
        };
        do {
            buttonSoundPlayer = try AVAudioPlayer(contentsOf: newURL)
            buttonSoundPlayer.numberOfLoops = loopCount
            
            buttonSoundPlayer.prepareToPlay()
            buttonSoundPlayer.play()
        } catch let error as NSError {
            print(error.description)
        }
    }
    
}

//A Background Music which runs in Background
var backgroundMusic = AVAudioPlayer()
let url = Bundle.main.url(forResource: "Music.mp3", withExtension: nil)
if let newURL = url {
    do {
        backgroundMusic = try AVAudioPlayer(contentsOf: newURL)
        backgroundMusic.numberOfLoops = -1
        
        backgroundMusic.prepareToPlay()
        backgroundMusic.play()
    } catch let error as NSError {
        print(error.description)
    }
}
else{
    print("Could not find Music File )")
}


//Setting up Background and it's animation in async thread
//Background
let bg = Rectangle(width: 140, height: 80, cornerRadius: 0)
bg.color = Color.green.withAlpha(alpha: 0.45)



//tortoise Logic
let tortoise = Image(name: "tortoise")
tortoise.size = Size(width: 20, height: 20)
tortoise.contentMode = .scaleToFitMaintainingAspectRatio

let tortoiseTimer = Timer.scheduledTimer(withTimeInterval: 10, repeats: true){_ in
    let y = Double(25-arc4random_uniform(10))
    let x = Double(arc4random_uniform(80))-40
    //setting current location for tortoise for smoother animation
    tortoise.center.y = 50
    tortoise.center.x = x+Double(arc4random_uniform(10))
    animate(duration: 7){
        tortoise.center.y = y
        tortoise.center.x = x
        tortoise.rotation = Double(arc4random_uniform(10)/10)
    }
    DispatchQueue.main.asyncAfter(deadline: .now()+7){
        animate(duration: 5){
            tortoise.center.y = 50
            tortoise.center.x = x + Double(arc4random_uniform(5))
            tortoise.rotation = Double(arc4random_uniform(10)/10)
        }
        
    }
    
    
}
tortoiseTimer.fire()

//wheel Logic
let wheel = Image(name: "wheel")
wheel.size = Size(width: 100, height: 100)
wheel.tint = Color(_colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.5)
wheel.center.y -= 20

var timePassed = 0.0
let timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true){_ in
    timePassed += 0.1
    wheel.rotation = timePassed/2
    //wheel.center.y -= timePassed
}
timer.fire()






var gameBegun = false







//Various Buttons
func HateToLove(){
    
    //declaration of all objects
    struct difficulty{
        var id: Int = 1
        var label: String = "Hello"
    }
    
    //titleBar
    let difficultyBgImg:Image = Image(name: "squareBgButton")
    difficultyBgImg.contentMode = .scaleAndStretchToFill
    var diff = difficulty()
    var difficultyLabel:Text = Text(string: "EASY", fontSize: 30, fontName: boldFontName, color: .black)
    let lowerDiffButton: Image = Image(name: "lowDiffButton")
    let highDiffButton:Image = Image(name: "highDiffButton")
    let colorButton = Image(name: "colorPicker")
    let skipButton = Image(name: "skipButton")
    let closeButton = Image(name: "closeButton")
    let hintButton = Image(name:"hintButton")
    
    //Target and current word box
    let targetWordLabel = Text(string: "LOVE", fontSize: 100, fontName: boldFontName, color: .black)
    var backgroundLayer = [Rectangle]()
    var currentWordLabel = [Text]()
    var currentWord = [Rectangle]()
    for _ in 1...4{
        backgroundLayer.append(Rectangle(width: 16, height: 16, cornerRadius: 1.6))
        currentWordLabel.append(Text(string: "A", fontSize: 100, fontName: boldFontName, color: .black))
        currentWord.append(Rectangle(width: 16, height: 16, cornerRadius: 1.6))
    }
    
    
    
    //Difficulty Logic
    var labelVisible = false
    func changeDifficulty(lower: Bool){
        let message = Text(string: "", fontSize: 20, fontName: boldFontName, color: .black)
        if lower == true{
            if diff.id != 1{
                diff.id -= 1
            }
            
        }
        else{
            if diff.id != 3{
                diff.id += 1
            }
        }
        let labels = ["EASY","MEDIUM","HARD"]
        diff.label = labels[diff.id-1]
        difficultyLabel.string = diff.label
        if labelVisible==false{
            message.string = "NEXT PUZZLE WILL BE "+labels[diff.id-1]
            labelVisible = true
            message.center.y -= 22
            DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                labelVisible = false
                message.remove()
            }
        }
        
        
    }
    
    highDiffButton.onTouchUp {
        if diff.id==3{
            playButtonSound(filename: "error.wav", loopCount: 0)
            
        }
        else{
            playButtonSound(filename: "buttonClick.wav", loopCount: 0)
            
        }
        changeDifficulty(lower: false)
    }
    lowerDiffButton.onTouchUp {
        if diff.id==1{
            playButtonSound(filename: "error.wav", loopCount: 0)
        }
        else{
            playButtonSound(filename: "errorLong.wav", loopCount: 0)
            
        }
        
        
        changeDifficulty(lower: true)
    }
    //put x-y position for all
    let titleBarYPosition = 24.5
    difficultyLabel.center.y = titleBarYPosition
    difficultyBgImg.center.y = titleBarYPosition
    highDiffButton.center.y = titleBarYPosition
    lowerDiffButton.center.y = titleBarYPosition
    colorButton.center.y = titleBarYPosition
    hintButton.center.y = titleBarYPosition
    skipButton.center.y = titleBarYPosition
    closeButton.center.y = titleBarYPosition
    
    lowerDiffButton.center.x = -9.5
    highDiffButton.center.x = 9.5
    
    colorButton.center.x = 22
    skipButton.center.x = 29
    hintButton.center.x = 36
    closeButton.center.x = 43
    targetWordLabel.center.y = 10
    
    targetWordLabel.center.x = -22
    for i in 0...3{
        currentWord[i].center.x = -30+Double(i*20)
        currentWordLabel[i].center.x = -30+Double(i*20)
        backgroundLayer[i].center.x = -30+Double(i*20)
        currentWord[i].center.y = -6
        currentWordLabel[i].center.y = -6
        backgroundLayer[i].center.y = -6
        
    }
    
    //updating scale and size
    difficultyBgImg.size = Size(width: 30, height: 8)
    lowerDiffButton.size = Size(width: 5, height: 4)
    highDiffButton.size = Size(width: 5, height: 4)
    
    let BUTTON_SIZE = Size(width: 6, height: 6)
    hintButton.size = BUTTON_SIZE
    colorButton.size = BUTTON_SIZE
    closeButton.size = BUTTON_SIZE
    skipButton.size = BUTTON_SIZE
    
    for i in 0...3{
        currentWord[i].borderWidth = 4
        backgroundLayer[i].borderWidth = 4
    }
    
    //Filling Colors
    let primaryColorA = Color.yellow
    let colorLight = Color.white
    
    func updateColors (colorDark: Color,colorLight: Color,primaryColorA: Color,primaryColorB: Color,primaryColorC: Color){
        difficultyLabel.color = colorDark
        targetWordLabel.color = colorDark
        for i in 0...3{
            currentWordLabel[i].color = colorDark
            currentWord[i].color = .clear
            currentWord[i].borderColor = colorDark
            backgroundLayer[i].color = .white
        }
        
    }
    updateColors(colorDark: .black, colorLight: .white, primaryColorA: .yellow, primaryColorB: .blue, primaryColorC: .orange)
    
    
    let modelURL = Bundle.main.url(forResource: "my_model_1010_new", withExtension: "mlmodelc")
    let model = try! MLModel(contentsOf: modelURL!)
    func predict(arr10: [Double])->String{
        let input = mlInput.init(char: arr10)
        let temp = try! prediction(model: model, input: input)
        let answer = topProbabilities(answer: temp)
        return answer[0]
    }
    
    func getArrayFromCircles(mainCircle:[Circle],otherCircle:[Circle],region:Rectangle,arrSize:Int)->[[Bool]]{
        var isBitMapOccupied = [[Bool]]()
        
        
        for _ in 0...arrSize-1{
            var temp = [Bool]()
            //isBitMapOccupied.append([])
            for _ in 0...arrSize-1{
                temp.append(true)
            }
            isBitMapOccupied.append(temp)
        }
        let w = region.size.width*1.20
        let h = region.size.height*1.20
        let X = region.center.x-(w/2)
        let Y = region.center.y-(h/2)
        for circle in mainCircle{
            let fx = ((circle.center.x-X)*Double(arrSize)/w)
            let fy = ((circle.center.y-Y)*Double(arrSize)/h)
            let finalY = Int(fx)
            let finalX = arrSize-1-Int(fy)
            if finalX>=0 && finalY<arrSize && finalY>=0 && finalY<arrSize{
                isBitMapOccupied[finalX][finalY]=false
            }
            
        }
        
        return isBitMapOccupied
        
        
    }
    
    var convertedCircle = [Circle]()
    func drawConvertedImage(arr:[[Bool]],arrSize:Int){
        for i in 0...arrSize-1{
            for j in 0...arrSize-1{
                if arr[i][j]==true{
                    convertedCircle.append(Circle(radius: 0.5))
                    convertedCircle[convertedCircle.count-1].center=Point(x: Double(i), y: Double(j))
                    convertedCircle[convertedCircle.count-1].color = .red
                }
                else{
                    convertedCircle.append(Circle(radius: 0.5))
                    convertedCircle[convertedCircle.count-1].center=Point(x: Double(i), y: Double(j))
                    convertedCircle[convertedCircle.count-1].color = .yellow
                }
            }
        }
    }
    
    func removeConvertedCircle(){
        for i in convertedCircle{
            i.remove()
        }
        convertedCircle.removeAll()
        
    }
    func printFromattedArray(arr: [[Bool]]){
        for i in arr{
            var str = ""
            for j in i{
                if j==false{
                    
                    str += "1"
                }
                else{
                    str += "0"
                }
            }
        }
    }
    func boolToDouble(arr:[[Bool]])->[Double]{
        var temp = [Double]()
        for row in arr{
            for i in row{
                if i==true{ //true 1
                    temp.append(0)
                }
                else{
                    temp.append(1)
                }
            }
        }
        return temp
    }
    func isPointInsideBox(point: Point,rect: Rectangle, dropDistance: Double,rad: Double )->Bool{
        if (point.x+rad > rect.center.x + ((rect.size.width)/2)) || (point.x-rad < rect.center.x - ((rect.size.width)/2)) || (point.y+rad > rect.center.y + ((rect.size.height)/2)) || (point.y-rad < rect.center.y - ((rect.size.height)/2)) {
            return false
        }
        else{
            return true
        }
    }
    
    func findDistance(a:Point,b:Point)->Double{
        return sqrt(((a.x-b.x)*(a.x-b.x))+((a.y-b.y)*(a.y-b.y)))
    }
    var mainCircles = [Circle]()
    var helperCircles = [Line]()
    var previous = Circle(radius: 0)
    
    //Drawing Logic
    
    func drawCircleUsingTouchpoint(_ point:Point,rect:Rectangle){
        
        if isPointInsideBox(point: point, rect: rect, dropDistance: 0, rad: 0.6){
            mainCircles.append(Circle(radius: 0.6))
            mainCircles[mainCircles.count-1].color = .black
            mainCircles[mainCircles.count-1].center = point
            if findDistance(a: mainCircles[mainCircles.count-1].center, b: previous.center)>0.6 && mainCircles.count>1{
                helperCircles.append(Line(start: mainCircles[mainCircles.count-1].center, end: previous.center, thickness: 1.2))
                helperCircles[helperCircles.count-1].color = .black
                
            }
            previous = mainCircles[mainCircles.count-1]
        }
        
        
    }
    
    
    //Actual Game Logic goes here (start word, current word, target word) and functionality related stuffs (Find next word, etc)
    
    var cword = "HATE"{
        didSet{
            for i in 0...3{
                currentWordLabel[i].string =  cword[i]
                currentWordLabel[i].color =  .black
                
            }
        }
    }
    
    var tword = "LOVE"{
        didSet{
            targetWordLabel.string = tword
            targetWordLabel.color = .black
        }
    }
    let graph = loadGraph()
    func generateGame(){
        let randomLevel = (diff.id * 2) + Int(arc4random_uniform(2))
        var foundPuzzle = false
        while(foundPuzzle==false){
            graph.refresh()
            let SolvedRandomPuzzle = nextNodeSuggestion(graph: graph, str1: findWord(atIndex: Int(arc4random_uniform(2339)) ), str2: findWord(atIndex: Int(arc4random_uniform(2339))))
            if SolvedRandomPuzzle.count > 1{
                foundPuzzle = true
                tword = SolvedRandomPuzzle.count >= randomLevel ? SolvedRandomPuzzle[randomLevel-1]: SolvedRandomPuzzle[SolvedRandomPuzzle.count-1]
                cword = SolvedRandomPuzzle[0]
                for i in 0...3{
                    currentWordLabel[i].color = .black
                    currentWordLabel[i].string = cword[i]
                }
                
            }
        }
    }
    
    
    
    for i in 0...3{
        currentWord[i].onTouchDrag {
            let points = currentWord[i].currentTouchPoints()
            if points.count>=1{
                drawCircleUsingTouchpoint(points[0],rect:currentWord[i])
            }
            
        }
    }
    for i in 0...3{
        currentWord[i].onTouchDown {
            currentWordLabel[i].color = currentWordLabel[i].color.withAlpha(alpha: 0.4)
        }
    }
    for i in 0...3{
        currentWord[i].onTouchUp {
            currentWordLabel[i].color = currentWordLabel[i].color.withAlpha(alpha: 1)
            
            //check validity of answer (is answer correct if yes, if game over and so)
            let imageArray = getArrayFromCircles(mainCircle: mainCircles, otherCircle: mainCircles, region: currentWord[i], arrSize: 10)
            let predictedWord = (predict(arr10: boolToDouble(arr: imageArray)))
            currentWordLabel[i].string = predictedWord
            
            //remove drawn word from space
            for j in mainCircles{
                j.remove()
            }
            for j in helperCircles{
                j.remove()
            }
            while  mainCircles.count>0 {
                mainCircles.remove(at: 0)
            }
            while  helperCircles.count>0 {
                helperCircles.remove(at: 0)
            }
            
            var typedWord = ""
            for j in 0...3{
                if j != i{
                    typedWord += cword[j]
                }
                else{
                    typedWord += predictedWord
                }
            }
            var error = false
            
            if wordIndex(word: typedWord) != -1{
                if checkDifference1(val1: cword, val2: typedWord) == false{
                    error = true
                    
                }
            }
            else{
                error = true
            }
            if error==true{
                for i in 0...3{
                    playButtonSound(filename: "invalid.wav", loopCount: 0)
                    currentWordLabel[i].color = .red
                }
                DispatchQueue.main.asyncAfter(deadline: .now()+0.7){
                    cword = cword
                }
            }
            else{
                for i in 0...3{
                    playButtonSound(filename: "winningSound.wav", loopCount: 0)
                    currentWordLabel[i].color = .green
                }
                if typedWord==tword{
                    //great, you won the game
                    playButtonSound(filename: "winLong.wav", loopCount: 0)
                    targetWordLabel.color = .green
                    DispatchQueue.main.asyncAfter(deadline: .now()+0.7){
                        graph.refresh()
                        
                        generateGame()
                    }
                    
                    
                }
                else{
                    DispatchQueue.main.asyncAfter(deadline: .now()+0.7){
                        cword = typedWord
                        
                    }
                }
                
                
            }
            
        }
    }
    
    
    
    generateGame()
    
    //clearing all shapes of game and get back to main screen
    closeButton.onTouchUp {
        playButtonSound(filename: "buttonClick.wav", loopCount: 0)
        difficultyBgImg.remove()
        difficultyLabel.remove()
        lowerDiffButton.remove()
        highDiffButton.remove()
        skipButton.remove()
        hintButton.remove()
        targetWordLabel.remove()
        for i in 0...3{
            currentWord[i].remove()
            currentWordLabel[i].remove()
            backgroundLayer[i].remove()
        }
        currentWord.removeAll()
        currentWordLabel.removeAll()
        backgroundLayer.removeAll()
        closeButton.remove()
        colorButton.remove()
        mainPage()
    }
    
    //hintButtonLogic
    hintButton.onTouchDown {
        playButtonSound(filename: "buttonClick.wav", loopCount: 0)
        hintButton.scale = 1.15
        hintButton.dropShadow = Shadow()
    }
    hintButton.onTouchUp {
        hintButton.scale = 1
        hintButton.dropShadow = nil
        graph.refresh()
        let suggestedWord = nextNodeSuggestion(graph: graph, str1: tword, str2: cword)
        let rectLabel = Text(string: suggestedWord[1], fontSize: 40, fontName: boldFontName, color: .black)
        rectLabel.center.x = hintButton.center.x
        rectLabel.center.y = hintButton.center.y - 8
        DispatchQueue.main.asyncAfter(deadline: .now()+1.25){
            rectLabel.remove()
        }
    }
    
    //skipButtonLogic
    skipButton.onTouchDown {
        playButtonSound(filename: "buttonClick.wav", loopCount: 0)
        skipButton.scale = 1.15
        skipButton.dropShadow = Shadow()
    }
    skipButton.onTouchUp {
        generateGame()
        skipButton.dropShadow = nil
        
    }
    
    colorButton.onTouchDown {
        playButtonSound(filename: "buttonClick.wav", loopCount: 0)
        colorButton.scale = 1.15
        colorButton.dropShadow = Shadow()
    }
    colorButton.onTouchUp {
        bg.color = Color.random().withAlpha(alpha: 0.45)
        colorButton.dropShadow = nil
        
    }
    
}

func mainPage(){
    let play = Image(name: "PlayButton")
    play.size = Size(width: 0, height: 0)
    
    var loadComplete = false{
        didSet{
            if loadComplete==true{
                animate(duration: 1, delay: 0.4){
                    play.tint = nil
                    play.size = Size(width: 30, height: 10 )
                    play.contentMode = .scaleToFitMaintainingAspectRatio
                    play.center.y -= 14
                    
                    
                }
                
            }
        }
    }
    
    
    var wordArrayText = [Text]()
    var boyImages = [Image]()
    var girlImages = [Image]()
    for i in 0...5{
        boyImages.append(Image(name: "boy"+String(i)))
        boyImages[i].center = Point(x: -300, y: -300)
        boyImages[i].size = Size(width: 30, height: 100)
        
        girlImages.append(Image(name: "girl"+String(i)))
        girlImages[i].center = Point(x: -300, y: -300)
        girlImages[i].size = Size(width: 30, height: 100)
        
    }
    var boyImage = boyImages[0]
    var girlImage = girlImages[0]
    
    
    var isSkipActive = true
    
    play.onTouchUp {
        playButtonSound(filename: "buttonClick.wav", loopCount: 0)
        for i in 0...3{
            wordArrayText[i].remove()
        }
        for i in 0...5{
            boyImages[i].remove()
            girlImages[i].remove()
        }
        wordArrayText.removeAll()
        boyImages.removeAll()
        girlImages.removeAll()
        play.remove()
        HateToLove()
    }
    
    //updates info Box and image
    func updateImageFor(state:Int,after:Double){
        DispatchQueue.main.asyncAfter(deadline: .now()+after){
            boyImage.center = Point(x: -300, y: -300)
            boyImage = boyImages[state]
            boyImage.dropShadow = Shadow()
            boyImage.center = Point(x: 35, y: 0)
            boyImage.contentMode = .scaleToFitMaintainingAspectRatio
            
            girlImage.center = Point(x: -300, y: -300)
            girlImage = girlImages[state]
            girlImage.center = Point(x: -35, y: 0)
            girlImage.contentMode = .scaleToFitMaintainingAspectRatio
            girlImage.dropShadow = Shadow()
        }
        
        
        
    }
    updateImageFor(state: 0,after:0)
    
    for i in 0...3{
        let word = "HATE"
        wordArrayText.append(Text(string: word[i], fontSize:
            150, fontName: boldFontName, color: .black))
        wordArrayText[i].center.x = -22.5+(Double(i)*15)
        wordArrayText[i].center.y += 10
        wordArrayText[i].color = .black
        
        
    }
    func updateBoxWith(word: String,time:Double){
        if word=="LOVE"{
            DispatchQueue.main.asyncAfter(deadline: .now() + time+0.7){
                loadComplete = true
            }
            
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + time+0.1) {
            for i in 0...3{
                if wordArrayText[i].string != word[i]{
                    wordArrayText[i].color = Color.blue.darker(percent: 200)
                }
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3 + time) {
            for i in 0...3{
                wordArrayText[i].string = word[i]
            }
            
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5+time) {
            for i in 0...3{
                wordArrayText[i].color = .black
            }
            
        }
        
    }
    
    for i in 1...5{
        updateImageFor(state: i, after: 0.3+((Double(i)-1)*0.7))
    }
    updateBoxWith(word: "LATE", time: 0.3)
    updateBoxWith(word: "LITE", time: 1.0)
    updateBoxWith(word: "LIFE", time: 1.7)
    updateBoxWith(word: "LIVE", time: 2.4)
    updateBoxWith(word: "LOVE", time: 3.1)
    
    
}
mainPage()








