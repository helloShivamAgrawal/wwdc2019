import Foundation
//created functions and implement data structures related graphs to implement game
public extension String {
    
    var length: Int {
        return count
    }
    //print("Hello")
    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }
    
    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }
    
    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }
    
    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[start ..< end])
    }
    
}
public func findDistance(graph: Graph, str1: String, str2: String)->Int{
    var distance = 1;
    let sourceID = wordIndex(word: str1)
    let destinationID = wordIndex(word: str2)
    
    let source = graph.node(sourceID)
    let destination = graph.node(destinationID)
    
    // bfs with level
    var queue = Queue<Node>()
    queue.enqueue(source)
    source.distance = 1
    source.visited = true
    
    
    while let node = queue.dequeue() {
        var finished = false
        for edge in node.neighbors {
            let neighborNode = edge.neighbor
            
            if !neighborNode.visited {
                queue.enqueue(neighborNode)
                neighborNode.distance = node.distance! + 1
                if(neighborNode.label == destination.label){
                    distance = neighborNode.distance!
                    return distance
                }
                neighborNode.visited = true
                
            }
        }
    }
    return distance
}

public func currentWord(word: [Character])->String{
    var str = ""
    for i in word{
        str += String(i)
    }
    return str
}
public func toCharArray(str: String)->[Character]{
    var char = [Character]()
    for i in str{
        char.append(i)
    }
    return char
}

public func random(_ source: Graph,_ difficulty: Int)->(String,String){
    ///This function finds start and end word based on the difficulty level.
    var random = arc4random_uniform(2339)
    let arr = loadWords()
    let start = arr[Int(random)]
    var endArr = [String]()
    //let endArr = wordWithLevel(graph: source, str1: start, level: difficulty)
    var i = 0 //To don't get into infinity loop
    while(endArr.count==0 && i<=50){
        endArr = wordWithLevel(graph: source, str1: start, level: difficulty) //returns cloud of all possible word with given difficulty
        i += 1
    }
    //avoid breaking in case we don't get any value
    if endArr.count==0 {
        return (arr[Int(random)],arr[Int(random)])
    }
    random = arc4random_uniform(UInt32(endArr.count))
    let end = endArr[Int(random)]
    return (start,end)
}
public func loadWords()->[String]{
    ///To read file and get them as an array at time of initialize.
    // reading files
    let url = Bundle.main.url(forResource: "word", withExtension: "txt")
    let txt = try! String(contentsOf:url!)
    
    var wordArr = txt.components(separatedBy: "\n")
    for i in 0...wordArr.count-1{
        let temp = wordArr[i].components(separatedBy: "\r")
        wordArr[i] = temp[0]
    }
    
    return wordArr
    
    
}

//get index of given word from words.txt file
public func wordIndex( word: String)->Int{
    ///returns index of given word
    let wordArr = loadWords()
    var temp = 0
    for i in wordArr{
        if i == word{
            return temp
        }
        temp+=1
    }
    return -1
    
}
//get word from given index from words.txt file
public func findWord(atIndex: Int)->String{
    ///find word at given given index
    let wordArr = loadWords()
    // var temp = 0
    if atIndex<wordArr.count-1 && atIndex>=0{
        return wordArr[atIndex]
    }
    else {
        return ""
    }
    
}


//utility function to load text file containg graph
public func loadGraph()->Graph{
    let url2 = Bundle.main.url(forResource: "connectedWords", withExtension: "txt")
    let txt2 = try! String(contentsOf:url2!)
    let arr = txt2.components(separatedBy: .init(charactersIn: "\n"))
    
    //create graph by putting all words as vertices and if there is different of one character between words edge between them is created
    var graph = Graph()
    for i in 0...arr.count-1{
        graph.addNode(String(i))
    }
    
    for i in arr {
        var arr2 = i.components(separatedBy: .init(charactersIn: " "))
        var node = Int(arr2[0]) ?? -1
        if node != -1{
            arr2.remove(at: 0)
        }
        for j in 0...arr2.count-1{
            let edge = Int(arr2[j]) ?? -1
            if(node >= 0 && edge >= 0){
                graph.addEdge(graph.node(node), neighbor: graph.node(edge))
            }
        }
        
        
    }
    return graph
    
}

//TODO: Give hint based on the word -- improvision if he previously he/she have chosen something try to avoid that
//print(graph)
//get suggestion for nextNode by finding path from current and target word
public func nextNodeSuggestion(graph: Graph, str1: String, str2: String)->[String]{
    let sourceID = wordIndex(word: str1)
    let destinationID = wordIndex(word: str2)
    
    let source = graph.node(sourceID)
    let destination = graph.node(destinationID)
    
    // bfs with level
    var queue = Queue<Node>()
    queue.enqueue(source)
    source.distance = 1
    source.visited = true
    var dict = Dictionary<String,String>()
    
    var result = [String]()
    while let node = queue.dequeue() {
        for edge in node.neighbors {
            
            let neighborNode = edge.neighbor
            
            if !neighborNode.visited {
                dict.updateValue(node.label, forKey: neighborNode.label)
                queue.enqueue(neighborNode)
                neighborNode.distance = node.distance! + 1
                if(neighborNode.label == destination.label){
                    //   print(dict.description)
                    var temp = neighborNode.label
                    result.append(findWord(atIndex: Int(neighborNode.label) ?? 0))
                    while(temp != source.label){
                        result.append(findWord(atIndex: Int(dict[temp]!) ?? 0))
                        //result.append(dict[temp]!)
                        temp = dict[temp]!
                    }
                    
                    return result
                }
                neighborNode.visited = true
                
            }
        }
    }
    return result
}
//function to get word from given difficulty level
public func wordWithLevel(graph: Graph, str1: String,level: Int)->[String]{
    //A utility function to find nodes at given depth
    let sourceID = wordIndex(word: str1)
    var levelWords = [String]()
    
    let source = graph.node(sourceID)
    //let destination = graph.node(destinationID)
    
    // bfs with level
    var queue = Queue<Node>()
    queue.enqueue(source)
    source.distance = 1
    source.visited = true
    var dict = Dictionary<String,String>()
    var result = [String]()
    while let node = queue.dequeue() {
        for edge in node.neighbors {
            let neighborNode = edge.neighbor
            
            if !neighborNode.visited {
                dict.updateValue(node.label, forKey: neighborNode.label)
                queue.enqueue(neighborNode)
                neighborNode.distance = node.distance! + 1
                if neighborNode.distance == level{
                    levelWords.append(findWord(atIndex: Int(neighborNode.label)!))
                }
                neighborNode.visited = true
                
            }
        }
    }
    
    return levelWords
}
public func checkDifference1(val1: String,val2:String)->Bool{
    // to check whether difference between two string is one letter or not
    var ans = false;
    var arr2 = Array(val2)
    var arr1 = Array(val1)
    var diff = 0;
    if(arr1.count == arr2.count && arr1.count>0){
        for i in 0...arr1.count-1{
            if arr1[i].hashValue != arr2[i].hashValue{
                diff = diff+1
            }
        }
    }
    ans = (diff==1)
    
    return ans
}
