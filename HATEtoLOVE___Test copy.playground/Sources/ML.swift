import Foundation

import Foundation
import UIKit
import CoreML
// this is file to incorporate ML

//we need multiarray input(784) array as input
@available(iOS 11.0, *)
public class mlInput : MLFeatureProvider {
    var input1 =  MLMultiArray.init()
    //var charIn = MLMultiArray.init()
    public var featureNames: Set<String> {
        get {
            return ["input1"]
        }
    }
    public func featureValue(for featureName: String) -> MLFeatureValue? {
        if (featureName == "input1") {
            return MLFeatureValue(multiArray: input1)
        }
        return nil
    }
    
    public init(char: [Double]) {
        var loop = 0;
        self.input1 = try! MLMultiArray(shape: [100], dataType: .double)
        for i in char {
            
            input1[loop] = NSNumber(floatLiteral: Double(i))
            loop+=1
        }
        //self.charIn = mlMultiArray
        
    }
}


//on output side we are getting multiarray of 26
public class mlOutput : MLFeatureProvider {
    var output1 =  MLMultiArray.init()
    //var charIn = MLMultiArray.init()
    public var featureNames: Set<String> {
        get {
            return ["output1"]
        }
    }
    public func featureValue(for featureName: String) -> MLFeatureValue? {
        if (featureName == "output1") {
            return MLFeatureValue(multiArray: output1)
        }
        return nil
    }
    
    public init(char: [Double]) {
        var loop = 0;
        self.output1 = try! MLMultiArray(shape: [26], dataType: .double)
        for i in char {
            
            output1[loop] = NSNumber(floatLiteral: Double(i))
            loop+=1
        }
        
    }
}
public func prediction(model:MLModel, input: mlInput) throws -> [Double] {
    let output1 = try model.prediction(from: input)
    let outputMultiarray:MLMultiArray = output1.featureValue(for: "output1")!.multiArrayValue!
    var loop = 0;
    var tempDouble = [Double]()
    for i in 0...25{
        tempDouble.append(outputMultiarray[i].doubleValue)
        loop+=1
    }
    
    
    return tempDouble
}

//function to test generate 10*10 array in pretty fromatted way
public func formattedString(_ arr: [Double])->String{
    var str = ""
    for i in 0...9{
        str+="\n"
        for j in 0...9{
            str += String(Int(arr[(i*10)+j]))
            
        }
    }
    return str
}

//get letters with top 5 probabilities
public func topProbabilities(answer: [Double])->[String]{
    let letters = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    var temp = answer.sorted()
    var str = [String]()
    for i in 0...4{
        str.append(letters[answer.index(of: temp[25-i])!])
    }
    print(str)
    return str
}


