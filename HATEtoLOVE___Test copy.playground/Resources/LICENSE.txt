Here I would like to credit creators of Music and some clean code about data structures I used to make this playground

#sound files
License.wav: http://creativecommons.org/licenses/by/3.0/
winningSound.wav: https://freesound.org/people/LittleRobotSoundFactory/sounds/270528/
winLong.wav: https://freesound.org/people/LittleRobotSoundFactory/sounds/274177/
buttonSound.wav: https://freesound.org/people/JarredGibb/sounds/219476/
Invalid.wav: https://freesound.org/people/Isaac200000/sounds/188013/
ErrorSound.wav: https://freesound.org/people/distillerystudio/sounds/327736/
Music.mp3: http://www.tannerhelland.com/music-directory/

#Files inside Graph Folder (Edge,Graph,Hashset,Node,Queue)
https://github.com/raywenderlich/swift-algorithm-club/blob/master/LICENSE.txt
(Notice is included at Head of each files)

#Dataset
https://www.kaggle.com/sachinpatel21/az-handwritten-alphabets-in-csv-format
CC0 License
